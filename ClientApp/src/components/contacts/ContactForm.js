import React from 'react';
import { Field, reduxForm } from 'redux-form';
import formFields from './formFIelds';
import ContactField from './ContactField';

const re = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

const validate = values => {
  const errors = {};

  if (!re.test(values.email)) {
    errors.email = 'The email address is invalid!';
  }

  formFields.forEach(({ label, name }) => {
    if (!values[name]) {
      errors[name] = `${label} is required!`;
    }
  });

  return errors;
};

const ContactForm = props => {
  const { handleSubmit, reset, submitting, edit, onCancel, loading } = props;

  const cancel = () => {
    reset();
    onCancel();
  };

  const renderFields = () => {
    return formFields.map(({ label, name, type, selectValues }) => (
      <Field
        key={name}
        component={ContactField}
        type={type}
        label={label}
        name={name}
        selectValues={selectValues}
        parse={value =>
          type === 'number' ? (!value ? null : Number(value)) : value
        }
      />
    ));
  };

  return loading ? (
    <div>Loading...</div>
  ) : (
    <div>
      <form onSubmit={handleSubmit}>
        <h3>{edit ? 'Edit' : 'Create'} Contact</h3>
        {renderFields()}
        <div className="text-right">
          <button
            className="btn btn-danger mr-1"
            type="button"
            onClick={cancel}>
            Cancel
          </button>
          <button
            className="btn btn-success"
            type="submit"
            disabled={submitting}>
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default reduxForm({
  form: 'contactForm',
  validate
})(ContactForm);
