import React from 'react';
import { Route } from 'react-router';
import Layout from './Layout';
import Contact from './contacts/Contact';
import ContactList from './contacts/ContactList';
import './custom.css';

export default () => (
  <Layout>
    <Route path="/" component={ContactList} exact={true} />
    <Route path="/edit/:id" component={Contact} />
    <Route path="/create" component={Contact} />
  </Layout>
);
