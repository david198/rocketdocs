import React, { Fragment, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { deleteContact, getContacts, sort } from '../../actions/index';

export default () => {
  const contacts = useSelector(state => state.contact.contacts);
  const sortOrder = useSelector(state => state.sortOrder);
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getContacts());
  }, [dispatch]);

  const onDelete = contact => {
    if (
      window.confirm(
        `Delete contact ${contact.lastName}, ${contact.firstName} ${contact.middleInitial}`
      )
    ) {
      dispatch(deleteContact(contact.id));
    }
  };

  const sortByColumn = field => {
    dispatch(
      sort(field, sortOrder.field === field ? !sortOrder.isReverse : false)
    );
  };

  const sortContacts = (a, b) => {
    if (sortOrder.isReverse) {
      [a, b] = [b, a];
    }

    switch (sortOrder.field) {
      case 'email':
        return a.email.localeCompare(b.email);
      case 'age':
        return a.age - b.age;
      case 'name':
      default:
        return `${a.lastName}, ${a.firstName} ${a.middleInitial}`.localeCompare(
          `${b.lastName}, ${b.firstName} ${b.middleInitial}`
        );
    }
  };

  const renderHeader = (title, name) => (
    <Fragment>
      <span className="pointer">{title}</span>{' '}
      <span
        dangerouslySetInnerHTML={{
          __html:
            sortOrder.field === name
              ? sortOrder.isReverse
                ? '&darr;'
                : '&uarr;'
              : null
        }}></span>
    </Fragment>
  );

  const renderContacts = () => {
    return contacts
      .slice()
      .sort(sortContacts)
      .map(c => (
        <tr key={c.id} style={{ color: `${c.hairColor}` }}>
          <td className="align-middle">
            {c.lastName}, {c.firstName} {c.middleInitial}
          </td>
          <td className="align-middle">{c.email}</td>
          <td className="align-middle">{c.age}</td>
          <td className="text-right">
            <i
              className="fas fa-pen pointer mr-3"
              aria-label="edit"
              onClick={() => history.push(`/edit/${c.id}`)}></i>
            <i
              className="fas fa-trash pointer"
              aria-label="delete"
              onClick={() => onDelete(c)}></i>
          </td>
        </tr>
      ));
  };

  return (
    <div className="container">
      <div className="float-right mb-2">
        <Link to={'/create'} className="btn btn-primary">
          New
        </Link>
      </div>
      <div className="table-responsive">
        <table className="table">
          <thead>
            <tr>
              <th scope="col" onClick={() => sortByColumn('name')}>
                {renderHeader('Full Name', 'name')}
              </th>
              <th scope="col" onClick={() => sortByColumn('email')}>
                {renderHeader('Email Address', 'email')}
              </th>
              <th scope="col" onClick={() => sortByColumn('age')}>
                {renderHeader('Age', 'age')}
              </th>
              <th className="text-right">Actions</th>
            </tr>
          </thead>
          <tbody>{renderContacts()}</tbody>
        </table>
      </div>
    </div>
  );
};
