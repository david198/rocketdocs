export default [
  { label: 'First Name', name: 'firstName', type: 'text' },
  { label: 'Middle Initial', name: 'middleInitial', type: 'text' },
  { label: 'Last Name', name: 'lastName', type: 'text' },
  { label: 'Email Address', name: 'email', type: 'email' },
  { label: 'Age', name: 'age', type: 'number' },
  {
    label: 'Hair Color',
    name: 'hairColor',
    type: 'select',
    selectValues: [
      { key: '', value: null },
      { key: 'Black', value: '#000000' },
      { key: 'Blonde', value: '#ffebcd' },
      { key: 'Blue', value: '#007bff' },
      { key: 'Brown', value: '#a52a2a' },
      { key: 'Gray', value: '#6c757d' },
      { key: 'Green', value: '#28a745' },
      { key: 'Orange', value: '#fd7e14' },
      { key: 'Pink', value: '#e83e8c' },
      { key: 'Red', value: '#dc3545' }
    ]
  }
];
