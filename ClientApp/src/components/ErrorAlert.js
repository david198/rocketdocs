import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { clearError } from '../actions';

export default () => {
  const error = useSelector(state => state.error);
  const dispatch = useDispatch();

  return error ? (
    <div
      className="alert alert-danger alert-dismissible fade show"
      role="alert">
      {error}
      <button
        type="button"
        className="close"
        data-dismiss="alert"
        aria-label="Close"
        onClick={() => dispatch(clearError())}>
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  ) : null;
};
