using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RocketDocs.Models;

namespace RocketDocs.Data
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ContactContext(serviceProvider.GetRequiredService<DbContextOptions<ContactContext>>()))
            {
                if (context.Contacts.Any())
                {
                    return;
                }

                context.Contacts.Add(
                    new Contact
                    {
                        Id = 1,
                        FirstName = "David",
                        MiddleInitial = "A",
                        LastName = "Speirs",
                        Email = "david@speirs.io",
                        Age = 34,
                        HairColor = "#a52a2a"
                    });

                context.SaveChanges();
            }
        }
    }
}