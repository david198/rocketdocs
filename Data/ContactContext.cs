using Microsoft.EntityFrameworkCore;
using RocketDocs.Models;

namespace RocketDocs.Data
{
    public class ContactContext : DbContext
    {
        public ContactContext(DbContextOptions<ContactContext> options) : base(options)
        {
        }

        public DbSet<Contact> Contacts { get; set; }
    }
}
