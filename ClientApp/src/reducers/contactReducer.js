import {
  ADD_CONTACT,
  CANCEL_EDIT,
  SAVE_CONTACT,
  DELETE_CONTACT,
  GET_CONTACTS,
  GET_CONTACT
} from '../actions/types';

const initialState = {
  contact: null,
  contacts: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ADD_CONTACT:
      return { contact: null, contacts: [...state.contacts, action.payload] };
    case SAVE_CONTACT:
      return {
        contact: null,
        contacts: state.contacts.map(c =>
          c.id === action.payload.id ? action.payload : c
        )
      };
    case CANCEL_EDIT:
      return { ...state, contact: null };
    case DELETE_CONTACT:
      return {
        ...state,
        contacts: state.contacts.filter(c => c.id !== action.payload.id)
      };
    case GET_CONTACT:
      return { ...state, contact: action.payload };
    case GET_CONTACTS:
      return {
        contact: null,
        contacts: action.payload
      };
    default:
      return state;
  }
}
