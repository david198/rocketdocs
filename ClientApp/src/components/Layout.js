import React, { Fragment } from 'react';
import { Container } from 'reactstrap';
import NavMenu from './NavMenu';
import ErrorAlert from './ErrorAlert';

export default props => (
  <Fragment>
    <NavMenu />
    <Container>
      {props.children}
      <ErrorAlert />
    </Container>
  </Fragment>
);
