import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Navbar, NavbarBrand } from 'reactstrap';

export default () => (
  <header>
    <Navbar className="ng-white border-bottom box-shadow mb-3" light>
      <Container>
        <NavbarBrand tag={Link} to={'/'}>
          RocketDocs Contacts
        </NavbarBrand>
      </Container>
    </Navbar>
  </header>
);
