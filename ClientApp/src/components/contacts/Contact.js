import React, { useEffect } from 'react';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import ContactForm from './ContactForm';
import { addContact, cancelEdit, saveContact, getContact } from '../../actions';

export default () => {
  let { id } = useParams();
  const contact = useSelector(state => state.contact.contact);
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    if (id) {
      dispatch(getContact(id, history));
    }
  }, [dispatch, id, history]);

  const onCancel = () => {
    if (contact) {
      dispatch(cancelEdit());
    }

    history.push('/');
  };

  const submitForm = values => {
    if (contact) {
      dispatch(saveContact(contact.id, values));
    } else {
      dispatch(addContact(values));
    }

    history.push('/');
  };

  const isEdit = location.pathname.includes('edit');

  return (
    <ContactForm
      onSubmit={submitForm}
      edit={isEdit}
      initialValues={contact}
      onCancel={onCancel}
      loading={isEdit && !contact}
    />
  );
};
