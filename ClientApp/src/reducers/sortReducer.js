import { SORT } from '../actions/types';

const initialState = {
  field: 'name',
  isReverse: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SORT:
      return action.payload;
    default:
      return state;
  }
}
