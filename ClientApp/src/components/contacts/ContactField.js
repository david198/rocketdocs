import React from 'react';

export default ({
  input,
  label,
  type,
  selectValues,
  meta: { error, touched }
}) => (
  <div className="form-group">
    <label>{label}</label>
    {type === 'select' ? (
      <select {...input} className="form-control">
        {selectValues.map(values => (
          <option key={values.key} value={values.value}>
            {values.key}
          </option>
        ))}
      </select>
    ) : (
      <input {...input} type={type} className="form-control" />
    )}
    <small className="text-danger">{touched && error}</small>
  </div>
);
