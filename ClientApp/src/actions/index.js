import {
  ADD_CONTACT,
  CANCEL_EDIT,
  DELETE_CONTACT,
  ERROR,
  GET_CONTACTS,
  GET_CONTACT,
  SAVE_CONTACT,
  SORT,
  CLEAR_ERROR
} from './types';

export const addContact = contact => async dispatch => {
  const response = await fetch('contact', {
    method: 'post',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(contact)
  });

  if (response.ok) {
    const data = await response.json();

    dispatch({ type: ADD_CONTACT, payload: data });
  } else {
    dispatch({ type: ERROR, payload: 'Couldn`t add contact.' });
  }
};

export const cancelEdit = () => async dispatch => {
  dispatch({ type: CANCEL_EDIT });
};

export const deleteContact = id => async dispatch => {
  const response = await fetch(`contact/${id}`, { method: 'delete' });

  if (response.ok) {
    const data = await response.json();

    dispatch({ type: DELETE_CONTACT, payload: data });
  } else {
    dispatch({ type: ERROR, payload: 'Couldn`t delete contact.' });
  }
};

export const getContact = (id, history) => async dispatch => {
  const response = await fetch(`contact/${id}`);

  if (response.ok) {
    const data = await response.json();

    dispatch({ type: GET_CONTACT, payload: data });
  } else {
    history.push('/');
    dispatch({ type: ERROR, payload: 'That contact doesn`t exist.' });
  }
};

export const getContacts = () => async dispatch => {
  const response = await fetch(`contact`);

  if (response.ok) {
    const data = await response.json();

    dispatch({ type: GET_CONTACTS, payload: data });
  } else {
    dispatch({ type: ERROR, payload: 'Couldn`t get a lit of contacts.' });
  }
};

export const saveContact = (id, contact) => async dispatch => {
  const response = await fetch(`contact/${id}`, {
    method: 'put',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(contact)
  });

  if (response.ok) {
    dispatch({ type: SAVE_CONTACT, payload: { id, ...contact } });
  } else {
    dispatch({ type: ERROR, payload: 'Couldn`t save the contact.' });
  }
};

export const sort = (field, isReverse) => async dispatch => {
  dispatch({ type: SORT, payload: { field, isReverse } });
};

export const clearError = () => async dispatch => {
  dispatch({ type: CLEAR_ERROR });
};
