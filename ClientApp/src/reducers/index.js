import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';
import contactReducer from './contactReducer';
import sortReducer from './sortReducer';
import errorReducer from './errorReducer';

export default combineReducers({
  form: reduxForm,
  contact: contactReducer,
  error: errorReducer,
  sortOrder: sortReducer
});
